
// Names of all the body parts, for finding the icons

$animeCustomBodyType1 = "f";
$animeCustomBodyType2 = "l";
$animeCustomBodyType3 = "m";

$animeCustomTorso_F_0 = "torsobasic";
$animeCustomTorso_F_1 = "torsoblazer";
$animeCustomTorso_F_2 = "torsoshirt";
$animeCustomTorso_F_3 = "torsoserafuku";
$animeCustomTorso_F_4 = "torsoserafukufull";
$animeCustomTorso_F_5 = "torsobikini";
$animeCustomTorso_F_6 = "torsomiku";
$animeCustomTorso_F_7 = "torsodress";

$animeCustomEyes_F_0 = "eyesbasic";
$animeCustomEyes_F_1 = "eyesangry";
$animeCustomEyes_F_2 = "eyesveryangry";
$animeCustomEyes_F_3 = "eyessleepy";
$animeCustomEyes_F_4 = "eyesyuki";
$animeCustomEyes_F_5 = "eyesOO";
$animeCustomEyes_F_6 = "eyesshock";
$animeCustomEyes_F_7 = "none";

$animeCustomHair_F_0 = "none";
$animeCustomHair_F_1 = "hairtwintails";
$animeCustomHair_F_2 = "hairtwosideup";
$animeCustomHair_F_3 = "hairponytail";
$animeCustomHair_F_4 = "hairlong";
$animeCustomHair_F_5 = "hairponytaillong";
$animeCustomHair_F_6 = "hairbob";
$animeCustomHair_F_7 = "hairruffle";

$animeCustomHat_F_0 = "none";
$animeCustomHat_F_1 = "hatbowribbon";
$animeCustomHat_F_2 = "hatribbons";
$animeCustomHat_F_3 = "hathairband";

$animeCustomHip_F_0 = "skirt";
$animeCustomHip_F_1 = "pants";

$animeCustomChest_F_0 = "none";

$animeCustomTorso_L_0 = "torsobasic";
$animeCustomTorso_L_1 = "torsope";
$animeCustomTorso_L_2 = "torsoshirt";
$animeCustomTorso_L_3 = "torsoserafuku";
$animeCustomTorso_L_4 = "torsoserafukufull";
$animeCustomTorso_L_5 = "torsoswimsuit";
$animeCustomTorso_L_6 = "torsomiku";
$animeCustomTorso_L_7 = "torsodress";

$animeCustomEyes_L_0 = "eyesbasic";
$animeCustomEyes_L_1 = "eyesangry";
$animeCustomEyes_L_2 = "eyesveryangry";
$animeCustomEyes_L_3 = "eyessleepy";
$animeCustomEyes_L_4 = "eyesyuki";
$animeCustomEyes_L_5 = "eyesOO";
$animeCustomEyes_L_6 = "eyesshock";
$animeCustomEyes_L_7 = "";

$animeCustomHair_L_0 = "";
$animeCustomHair_L_1 = "hairtwintails";
$animeCustomHair_L_2 = "hairtwosideup";
$animeCustomHair_L_3 = "hairponytail";
$animeCustomHair_L_4 = "hairlong";
$animeCustomHair_L_5 = "hairponytaillong";
$animeCustomHair_L_6 = "hairpigtails";
$animeCustomHair_L_7 = "hairruffle";

$animeCustomHat_L_0 = "";
$animeCustomHat_L_1 = "hatbowribbon";
$animeCustomHat_L_2 = "hatribbons";
$animeCustomHat_L_3 = "hathat";

$animeCustomHip_L_0 = "skirt";
$animeCustomHip_L_1 = "pants";

$animeCustomChest_L_0 = "none";
$animeCustomChest_L_1 = "randoseru";

$animeCustomTorso_M_0 = "torsobasic";
$animeCustomTorso_M_1 = "torsoblazer";
$animeCustomTorso_M_2 = "torsoshirt";
$animeCustomTorso_M_3 = "torsogakuran";
$animeCustomTorso_M_4 = "torsoopen";
$animeCustomTorso_M_5 = "torsoshirtless";
$animeCustomTorso_M_6 = "torsoflowing";
$animeCustomTorso_M_7 = "torsobutler";

$animeCustomEyes_M_0 = "eyesbasic";
$animeCustomEyes_M_1 = "eyesangry";
$animeCustomEyes_M_2 = "eyesveryangry";
$animeCustomEyes_M_3 = "eyessleepy";
$animeCustomEyes_M_4 = "eyesyuki";
$animeCustomEyes_M_5 = "eyesOO";
$animeCustomEyes_M_6 = "eyesshock";
$animeCustomEyes_M_7 = "";

$animeCustomHair_M_0 = "";
$animeCustomHair_M_1 = "hairshounen";
$animeCustomHair_M_2 = "hairshounen2";
$animeCustomHair_M_3 = "hairemo";
$animeCustomHair_M_4 = "hairmullet";
$animeCustomHair_M_5 = "hairold";
$animeCustomHair_M_6 = "hairside";
$animeCustomHair_M_7 = "hairbuzz";

$animeCustomHat_M_0 = "";
$animeCustomHat_M_1 = "hatfrontbangs";
$animeCustomHat_M_2 = "hatheadband";
$animeCustomHat_M_3 = "hatnosebleed";

$animeCustomHip_M_0 = "pants";
$animeCustomHip_M_1 = "skirt";

$animeCustomChest_M_0 = "none";

// Add buttons in the avatar GUI to switch player types

if(!$AvatarCustomButtonsAdded){
	%letters = "DFLM";
	for(%c=0;%c<strLen(%letters);%c++){
		%key=getSubStr(%letters,%c,1);
		%but=new GuiBitmapButtonCtrl(){
			profile="BlockButtonProfile";
			horizSizing="right";
			vertSizing="bottom";
			position=(444 + %c*20)@ " 441";
			extent="19 19";
			minExtent="8 2";
			enabled="1";
			visible="1";
			clipToParent="1";
			command="Avatar_ClickCustomPlayertype("@(%c)@ ");";
			accelerator=strLwr(%key);
			text=strUpr(%key);
			groupNum="-1";
			buttonType="PushButton";
			bitmap="base/client/ui/button2";
			lockAspectRatio="0";
			alignLeft="0";
			alignTop="0";
			overflowImage="0";
			mKeepCached="0";
			mColor="255 255 255 255";
		};
		Avatar_Window.add(%but);
	}
	$AvatarCustomButtonsAdded = 1;
}

function Avatar_ClickCustomPlayertype(%c){
	if(%c!=$Pref::Player::CustomPlayertype){
		$Pref::Player::CustomPlayertype = %c;
		AvatarGui::onWake();
	}
}

// List of all nodes, because you have to hide them all one by one in the avatar preview

$acpNodesFemaleCount = -1;
$acpNodesFemale[$acpNodesFemaleCount++] = "lArm";
$acpNodesFemale[$acpNodesFemaleCount++] = "lHand";
$acpNodesFemale[$acpNodesFemaleCount++] = "lfinger";
$acpNodesFemale[$acpNodesFemaleCount++] = "rHand";
$acpNodesFemale[$acpNodesFemaleCount++] = "rfinger";
$acpNodesFemale[$acpNodesFemaleCount++] = "rArm";
$acpNodesFemale[$acpNodesFemaleCount++] = "BikiniRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "BlazerRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "MikuRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "Pants";
$acpNodesFemale[$acpNodesFemaleCount++] = "SerafukuRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "ShirtRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoBasic";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoBikini";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoBlazer";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoSerafuku";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoSerafukuFull";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoShirt";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesAngry";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesBasic";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesOO";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesShock";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesSleepy";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesVeryAngry";
$acpNodesFemale[$acpNodesFemaleCount++] = "EyesYuki";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairBob";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairLong";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairPonytail";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairPonytailLong";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairRuffle";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairTwintails";
$acpNodesFemale[$acpNodesFemaleCount++] = "HairTwoSideUp";
$acpNodesFemale[$acpNodesFemaleCount++] = "HatBowRibbon";
$acpNodesFemale[$acpNodesFemaleCount++] = "HatHairband";
$acpNodesFemale[$acpNodesFemaleCount++] = "HatRibbons";
$acpNodesFemale[$acpNodesFemaleCount++] = "HeadSkin";
$acpNodesFemale[$acpNodesFemaleCount++] = "PonytailPin";
$acpNodesFemale[$acpNodesFemaleCount++] = "TwintailsPin";
$acpNodesFemale[$acpNodesFemaleCount++] = "TwoSideUpPin";
$acpNodesFemale[$acpNodesFemaleCount++] = "lShoe";
$acpNodesFemale[$acpNodesFemaleCount++] = "rShoe";
$acpNodesFemale[$acpNodesFemaleCount++] = "Skirt";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoDress";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoMiku";
$acpNodesFemale[$acpNodesFemaleCount++] = "TorsoNeck";

$acpNodesLoliCount = -1;
$acpNodesLoli[$acpNodesLoliCount++] = "lArm";
$acpNodesLoli[$acpNodesLoliCount++] = "lHand";
$acpNodesLoli[$acpNodesLoliCount++] = "lfinger";
$acpNodesLoli[$acpNodesLoliCount++] = "rHand";
$acpNodesLoli[$acpNodesLoliCount++] = "rfinger";
$acpNodesLoli[$acpNodesLoliCount++] = "rArm";
$acpNodesLoli[$acpNodesLoliCount++] = "MikuRibbon";
$acpNodesLoli[$acpNodesLoliCount++] = "Pants";
$acpNodesLoli[$acpNodesLoliCount++] = "Randoseru";
$acpNodesLoli[$acpNodesLoliCount++] = "SerafukuRibbon";
$acpNodesLoli[$acpNodesLoliCount++] = "ShirtRibbon";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoBasic";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoPE";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoSerafuku";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoSerafukuFull";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoShirt";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoSwimsuit";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesAngry";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesBasic";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesOO";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesShock";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesSleepy";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesVeryAngry";
$acpNodesLoli[$acpNodesLoliCount++] = "EyesYuki";
$acpNodesLoli[$acpNodesLoliCount++] = "HairLong";
$acpNodesLoli[$acpNodesLoliCount++] = "HairPigtails";
$acpNodesLoli[$acpNodesLoliCount++] = "HairPonytail";
$acpNodesLoli[$acpNodesLoliCount++] = "HairPonytailLong";
$acpNodesLoli[$acpNodesLoliCount++] = "HairRuffle";
$acpNodesLoli[$acpNodesLoliCount++] = "HairTwintails";
$acpNodesLoli[$acpNodesLoliCount++] = "HairTwoSideUp";
$acpNodesLoli[$acpNodesLoliCount++] = "HatBowRibbon";
$acpNodesLoli[$acpNodesLoliCount++] = "HatHat";
$acpNodesLoli[$acpNodesLoliCount++] = "HatRibbons";
$acpNodesLoli[$acpNodesLoliCount++] = "HeadSkin";
$acpNodesLoli[$acpNodesLoliCount++] = "PonytailPin";
$acpNodesLoli[$acpNodesLoliCount++] = "TwintailsPin";
$acpNodesLoli[$acpNodesLoliCount++] = "TwoSideUpPin";
$acpNodesLoli[$acpNodesLoliCount++] = "lShoe";
$acpNodesLoli[$acpNodesLoliCount++] = "rShoe";
$acpNodesLoli[$acpNodesLoliCount++] = "Skirt";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoDress";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoMiku";
$acpNodesLoli[$acpNodesLoliCount++] = "TorsoNeck";

$acpNodesMaleCount = -1;
$acpNodesMale[$acpNodesMaleCount++] = "LArm";
$acpNodesMale[$acpNodesMaleCount++] = "LHand";
$acpNodesMale[$acpNodesMaleCount++] = "RArm";
$acpNodesMale[$acpNodesMaleCount++] = "RHand";
$acpNodesMale[$acpNodesMaleCount++] = "BlazerRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "FlowingRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "GakuranOpenRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "GakuranRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "OpenRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "Pants";
$acpNodesMale[$acpNodesMaleCount++] = "ShirtRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "ShirtlessRibbon";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoBlazer";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoButler";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoGakuran";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoNeck";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoOpen";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoShirt";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoShirtless";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoTShirt";
$acpNodesMale[$acpNodesMaleCount++] = "Torsobasic";
$acpNodesMale[$acpNodesMaleCount++] = "EyesAngry";
$acpNodesMale[$acpNodesMaleCount++] = "EyesBasic";
$acpNodesMale[$acpNodesMaleCount++] = "EyesOO";
$acpNodesMale[$acpNodesMaleCount++] = "EyesShock";
$acpNodesMale[$acpNodesMaleCount++] = "EyesSleepy";
$acpNodesMale[$acpNodesMaleCount++] = "EyesVeryAngry";
$acpNodesMale[$acpNodesMaleCount++] = "EyesYuki";
$acpNodesMale[$acpNodesMaleCount++] = "HairBuzz";
$acpNodesMale[$acpNodesMaleCount++] = "HairEmo";
$acpNodesMale[$acpNodesMaleCount++] = "HairMullet";
$acpNodesMale[$acpNodesMaleCount++] = "HairOld";
$acpNodesMale[$acpNodesMaleCount++] = "HairShounen";
$acpNodesMale[$acpNodesMaleCount++] = "HairShounen2";
$acpNodesMale[$acpNodesMaleCount++] = "HairSide";
$acpNodesMale[$acpNodesMaleCount++] = "HatFrontBangs";
$acpNodesMale[$acpNodesMaleCount++] = "HatHeadband";
$acpNodesMale[$acpNodesMaleCount++] = "HatNosebleed";
$acpNodesMale[$acpNodesMaleCount++] = "Headskin";
$acpNodesMale[$acpNodesMaleCount++] = "OldPin";
$acpNodesMale[$acpNodesMaleCount++] = "LShoe";
$acpNodesMale[$acpNodesMaleCount++] = "RShoe";
$acpNodesMale[$acpNodesMaleCount++] = "Skirt";
$acpNodesMale[$acpNodesMaleCount++] = "TorsoFlowing";

// Mostly copypasted from the add-on, hides and unhides the appropriate nodes for a given appearance setting

function acpFixFemale(){
	for(%nodeidx=1; %nodeidx<=$acpNodesFemaleCount; %nodeidx++){
		%node = $acpNodesFemale[%nodeidx];
		Avatar_Preview.hideNode("", %node);
	}
	
	Avatar_Preview.unHideNode("", "headSkin");
	Avatar_Preview.unHideNode("", "TorsoBasic");
	Avatar_Preview.unHideNode("", "Skirt");
	Avatar_Preview.unHideNode("", "rArm");
	Avatar_Preview.unHideNode("", "rShoe");
	Avatar_Preview.unHideNode("", "rHand");
	Avatar_Preview.unHideNode("", "lArm");
	Avatar_Preview.unHideNode("", "lShoe");
	Avatar_Preview.unHideNode("", "lHand");
	%hasRibbon = 0;
	%hasPin = 0;
	
	Avatar_Preview.schedule(0, setNodeColor, "", "TorsoBasic", $Pref::Avatar::TorsoColor);
	
	if($Pref::Avatar::pack!=0){
		if($Pref::Avatar::pack==1){
			%pack = "torsoblazer";
			%hasRibbon = 1;
			%ribbon = "blazerribbon";
		}
		if($Pref::Avatar::pack==2){
			%pack = "torsoshirt";
			%hasRibbon = 1;
			%ribbon = "shirtribbon";
		}
		if($Pref::Avatar::pack==3){
			%pack = "torsoserafuku";
			%hasRibbon = 1;
			%ribbon = "serafukuribbon";
		}
		if($Pref::Avatar::pack==4){
			%pack = "torsoserafukuFull";
			%hasRibbon = 1;
			%ribbon = "serafukuribbon";
		}
		if($Pref::Avatar::pack==5){
			%pack = "bikiniribbon";
		}
		if($Pref::Avatar::pack==6){
			%pack = "torsoMiku";
			%hasRibbon = 1;
			%ribbon = "mikuribbon";
		}
		if($Pref::Avatar::pack==7){
			%pack = "torsoDress";
			Avatar_Preview.HideNode("", "Skirt");
		}
		Avatar_Preview.hideNode("", "TorsoBasic");
		Avatar_Preview.unHideNode("", "TorsoNeck");
		Avatar_Preview.setNodeColor("", "TorsoNeck", $Pref::Avatar::TorsoColor);
		Avatar_Preview.unHideNode("", %pack);
		Avatar_Preview.setNodeColor("", %pack, $Pref::Avatar::packColor);
		if(%hasRibbon==1){
			Avatar_Preview.unHideNode("", %ribbon);
			Avatar_Preview.setNodeColor("", %ribbon, $Pref::Avatar::accentColor);
		}
		if($Pref::Avatar::pack==5){
			Avatar_Preview.HideNode("", "TorsoNeck");
			Avatar_Preview.unHideNode("", "TorsoBikini");
			Avatar_Preview.setNodeColor("", "TorsoBikini", $Pref::Avatar::TorsoColor);
		}
	}
	
	if($Pref::Avatar::secondpack!=7){
		if($Pref::Avatar::secondpack==0){
			%eyes = "eyesbasic";
		}
		if($Pref::Avatar::secondpack==1){
			%eyes = "eyesangry";
		}
		if($Pref::Avatar::secondpack==2){
			%eyes = "eyesveryangry";
		}
		if($Pref::Avatar::secondpack==3){
			%eyes = "eyessleepy";
		}
		if($Pref::Avatar::secondpack==4){
			%eyes = "eyesyuki";
		}
		if($Pref::Avatar::secondpack==5){
			%eyes = "eyesOO";
		}
		if($Pref::Avatar::secondpack==6){
			%eyes = "eyesshock";
		}
		Avatar_Preview.unHideNode("", %eyes);
		Avatar_Preview.setNodeColor("", %eyes, $Pref::Avatar::secondpackColor);
	}
	
	if($Pref::Avatar::hat!=0){
		if($Pref::Avatar::hat==1){
			%hair="HairTwintails";
			%hasPin = 1;
			%pin="TwintailsPin";
		}
		if($Pref::Avatar::hat==2){
			%hair="HairTwoSideUp";
			%hasPin = 1;
			%pin="TwoSideUpPin";
		}
		if($Pref::Avatar::hat==3){
			%hair="HairPonytail";
			%hasPin = 1;
			%pin="PonytailPin";
		}
		if($Pref::Avatar::hat==4){
			%hair="HairLong";
		}
		if($Pref::Avatar::hat==5){
			%hair="HairPonytailLong";
		}
		if($Pref::Avatar::hat==6){
			%hair="HairBob";
		}
		if($Pref::Avatar::hat==7){
			%hair="HairRuffle";
		}
		Avatar_Preview.unHideNode("", %hair);
		Avatar_Preview.setNodeColor("", %hair, $Pref::Avatar::hatColor);
		if(%hasPin==1){
			Avatar_Preview.unHideNode("", %pin);
			Avatar_Preview.setNodeColor("", %pin, $Pref::Avatar::accentColor);
		}
	}
	
	if($Pref::Avatar::accent!=0){
		if($Pref::Avatar::accent==1){
			%accent = "HatBowRibbon";
		}
		if($Pref::Avatar::accent==2){
			%accent = "HatRibbons";
		}
		if($Pref::Avatar::accent==3){
			%accent = "HatHairband";
		}
		
		Avatar_Preview.unHideNode("", %accent);
		Avatar_Preview.setNodeColor("", %accent, $Pref::Avatar::accentColor);
	}
	
	if($Pref::Avatar::hip==1){
		Avatar_Preview.HideNode("", "skirt");
		Avatar_Preview.unHideNode("", "pants");
		Avatar_Preview.setNodeColor("", "pants", $Pref::Avatar::hipColor);
	}
	
	Avatar_Preview.setNodeColor("", "headSkin", $Pref::Avatar::headColor);
	
	Avatar_Preview.setNodeColor("", "torsobasic", $Pref::Avatar::chestColor);
	Avatar_Preview.setNodeColor("", "skirt", $Pref::Avatar::hipColor);
	
	Avatar_Preview.setNodeColor("", "rArm", $Pref::Avatar::rarmColor);
	Avatar_Preview.setNodeColor("", "lArm", $Pref::Avatar::larmColor);
	
	Avatar_Preview.setNodeColor("", "rHand", $Pref::Avatar::rhandColor);
	Avatar_Preview.setNodeColor("", "lHand", $Pref::Avatar::lhandColor);
	
	Avatar_Preview.setNodeColor("", "rShoe", $Pref::Avatar::rlegColor);
	Avatar_Preview.setNodeColor("", "lShoe", $Pref::Avatar::llegColor);
}

function acpFixLoli(%pl, %cl){
	for(%nodeidx=1; %nodeidx<=$acpNodesLoliCount; %nodeidx++){
		%node = $acpNodesLoli[%nodeidx];
		Avatar_Preview.hideNode("", %node);
	}
	
	Avatar_Preview.unHideNode("", "headSkin");
	Avatar_Preview.unHideNode("", "TorsoBasic");
	Avatar_Preview.unHideNode("", "Skirt");
	Avatar_Preview.unHideNode("", "rArm");
	Avatar_Preview.unHideNode("", "rShoe");
	Avatar_Preview.unHideNode("", "rHand");
	Avatar_Preview.unHideNode("", "lArm");
	Avatar_Preview.unHideNode("", "lShoe");
	Avatar_Preview.unHideNode("", "lHand");
	%hasRibbon = 0;
	%hasPin = 0;
	
	Avatar_Preview.schedule(0, setNodeColor, "", "TorsoBasic", $Pref::Avatar::TorsoColor);
	
	if($Pref::Avatar::chest==1){
		Avatar_Preview.unHideNode("", "randoseru");
		Avatar_Preview.setNodeColor("", "randoseru", "1 0 0 1");
	}
	
	if($Pref::Avatar::pack != 0){
		if($Pref::Avatar::pack==1){
			%pack = "torsoPE";
		}
		if($Pref::Avatar::pack==2){
			%pack = "torsoshirt";
			%hasRibbon = 1;
			%ribbon = "shirtribbon";
		}
		if($Pref::Avatar::pack==3){
			%pack = "torsoserafuku";
			%hasRibbon = 1;
			%ribbon = "serafukuribbon";
		}
		if($Pref::Avatar::pack==4){
			%pack = "torsoserafukuFull";
			%hasRibbon = 1;
			%ribbon = "serafukuribbon";
		}
		if($Pref::Avatar::pack==5){
			%pack = "torsoswimsuit";
		}
		if($Pref::Avatar::pack==6){
			%pack = "torsoMiku";
			%hasRibbon = 1;
			%ribbon = "mikuribbon";
		}
		if($Pref::Avatar::pack==7){
			%pack = "torsoDress";
			Avatar_Preview.HideNode("", "Skirt");
		}
		Avatar_Preview.hideNode("", "TorsoBasic");
		Avatar_Preview.unHideNode("", "TorsoNeck");
		Avatar_Preview.setNodeColor("", "TorsoNeck", $Pref::Avatar::chestColor);
		Avatar_Preview.unHideNode("", %pack);
		Avatar_Preview.setNodeColor("", %pack, $Pref::Avatar::packColor);
		if(%hasRibbon==1){
			Avatar_Preview.unHideNode("", %ribbon);
			Avatar_Preview.setNodeColor("", %ribbon, $Pref::Avatar::accentColor);
		}
	}
	
	if($Pref::Avatar::secondpack != 7){
		if($Pref::Avatar::secondpack==0){
			%eyes = "eyesbasic";
		}
		if($Pref::Avatar::secondpack==1){
			%eyes = "eyesangry";
		}
		if($Pref::Avatar::secondpack==2){
			%eyes = "eyesveryangry";
		}
		if($Pref::Avatar::secondpack==3){
			%eyes = "eyessleepy";
		}
		if($Pref::Avatar::secondpack==4){
			%eyes = "eyesyuki";
		}
		if($Pref::Avatar::secondpack==5){
			%eyes = "eyesOO";
		}
		if($Pref::Avatar::secondpack==6){
			%eyes = "eyesshock";
		}
		Avatar_Preview.unHideNode("", %eyes);
		Avatar_Preview.setNodeColor("", %eyes, $Pref::Avatar::secondpackColor);
	}
	
	if($Pref::Avatar::hat != 0){
		if($Pref::Avatar::hat==1){
			%hair="HairTwintails";
			%hasPin = 1;
			%pin="TwintailsPin";
		}
		if($Pref::Avatar::hat==2){
			%hair="HairTwoSideUp";
			%hasPin = 1;
			%pin="TwoSideUpPin";
		}
		if($Pref::Avatar::hat==3){
			%hair="HairPonytail";
			%hasPin = 1;
			%pin="PonytailPin";
		}
		if($Pref::Avatar::hat==4){
			%hair="HairLong";
		}
		if($Pref::Avatar::hat==5){
			%hair="HairPonytailLong";
		}
		if($Pref::Avatar::hat==6){
			%hair="HairPigtails";
		}
		if($Pref::Avatar::hat==7){
			%hair="HairRuffle";
		}
		Avatar_Preview.unHideNode("", %hair);
		Avatar_Preview.setNodeColor("", %hair, $Pref::Avatar::hatColor);
		if(%hasPin==1){
			Avatar_Preview.unHideNode("", %pin);
			Avatar_Preview.setNodeColor("", %pin, $Pref::Avatar::accentColor);
		}
	}
	
	if($Pref::Avatar::accent != 0){
		if($Pref::Avatar::accent==1){
			%accent = "HatBowRibbon";
		}
		if($Pref::Avatar::accent==2){
			%accent = "HatRibbons";
		}
		if($Pref::Avatar::accent==3){
			%accent = "HatHat";
		}
		
		Avatar_Preview.unHideNode("", %accent);
		Avatar_Preview.setNodeColor("", %accent, $Pref::Avatar::accentColor);
	}
	
	if($Pref::Avatar::hip==1){
		Avatar_Preview.HideNode("", "skirt");
		Avatar_Preview.unHideNode("", "pants");
		Avatar_Preview.setNodeColor("", "pants", $Pref::Avatar::hipColor);
	}
	
	Avatar_Preview.setNodeColor("", "headSkin", $Pref::Avatar::headColor);
	
	Avatar_Preview.setNodeColor("", "torsobasic", $Pref::Avatar::chestColor);
	Avatar_Preview.setNodeColor("", "skirt", $Pref::Avatar::hipColor);
	
	Avatar_Preview.setNodeColor("", "rArm", $Pref::Avatar::rarmColor);
	Avatar_Preview.setNodeColor("", "lArm", $Pref::Avatar::larmColor);
	
	Avatar_Preview.setNodeColor("", "rHand", $Pref::Avatar::rhandColor);
	Avatar_Preview.setNodeColor("", "lHand", $Pref::Avatar::lhandColor);
	
	Avatar_Preview.setNodeColor("", "rShoe", $Pref::Avatar::rlegColor);
	Avatar_Preview.setNodeColor("", "lShoe", $Pref::Avatar::llegColor);
}

function acpFixMale(%pl, %cl){
	for(%nodeidx=1; %nodeidx<=$acpNodesMaleCount; %nodeidx++){
		%node = $acpNodesMale[%nodeidx];
		Avatar_Preview.hideNode("", %node);
	}
	
	Avatar_Preview.unHideNode("", "headSkin");
	Avatar_Preview.unHideNode("", "TorsoBasic");
	Avatar_Preview.unHideNode("", "Pants");
	Avatar_Preview.unHideNode("", "rArm");
	Avatar_Preview.unHideNode("", "rShoe");
	Avatar_Preview.unHideNode("", "rHand");
	Avatar_Preview.unHideNode("", "lArm");
	Avatar_Preview.unHideNode("", "lShoe");
	Avatar_Preview.unHideNode("", "lHand");
	%hasRibbon = 0;
	%hasPin = 0;
	
	Avatar_Preview.schedule(0, setNodeColor, "", "TorsoBasic", $Pref::Avatar::TorsoColor);
	
	if($Pref::Avatar::pack != 0){
		if($Pref::Avatar::pack==1){
			%pack = "torsoblazer";
			%hasRibbon = 1;
			%ribbon = "blazerribbon";
		}
		if($Pref::Avatar::pack==2){
			%pack = "torsoshirt";
			%hasRibbon = 1;
			%ribbon = "shirtribbon";
		}
		if($Pref::Avatar::pack==3){
			%pack = "torsogakuran";
			%hasRibbon = 1;
			%ribbon = "gakuranribbon";
		}
		if($Pref::Avatar::pack==4){
			%pack = "torsoopen";
			%hasRibbon = 1;
			%ribbon = "openribbon";
		}
		if($Pref::Avatar::pack==5){
			%pack = "shirtlessribbon";
		}
		if($Pref::Avatar::pack==6){
			%pack = "torsoFlowing";
			%hasRibbon = 1;
			%ribbon = "Flowingribbon";
		}
		if($Pref::Avatar::pack==7){
			%pack = "torsoButler";
		}
		Avatar_Preview.hideNode("", "TorsoBasic");
		Avatar_Preview.unHideNode("", "TorsoNeck");
		Avatar_Preview.setNodeColor("", "TorsoNeck", $Pref::Avatar::TorsoColor);
		Avatar_Preview.unHideNode("", %pack);
		Avatar_Preview.setNodeColor("", %pack, $Pref::Avatar::packColor);
		if(%hasRibbon==1){
			Avatar_Preview.unHideNode("", %ribbon);
			Avatar_Preview.setNodeColor("", %ribbon, $Pref::Avatar::accentColor);
		}
		if($Pref::Avatar::pack==5){
			Avatar_Preview.HideNode("", "TorsoNeck");
			Avatar_Preview.unHideNode("", "TorsoShirtless");
			Avatar_Preview.schedule(0, setNodeColor, "", "TorsoShirtless", $Pref::Avatar::TorsoColor);
		}
	}
	
	if($Pref::Avatar::secondpack != 7){
		if($Pref::Avatar::secondpack==0){
			%eyes = "eyesbasic";
		}
		if($Pref::Avatar::secondpack==1){
			%eyes = "eyesangry";
		}
		if($Pref::Avatar::secondpack==2){
			%eyes = "eyesveryangry";
		}
		if($Pref::Avatar::secondpack==3){
			%eyes = "eyessleepy";
		}
		if($Pref::Avatar::secondpack==4){
			%eyes = "eyesyuki";
		}
		if($Pref::Avatar::secondpack==5){
			%eyes = "eyesOO";
		}
		if($Pref::Avatar::secondpack==6){
			%eyes = "eyesshock";
		}
		Avatar_Preview.unHideNode("", %eyes);
		Avatar_Preview.setNodeColor("", %eyes, $Pref::Avatar::secondpackColor);
	}
	
	if($Pref::Avatar::hat != 0){
		if($Pref::Avatar::hat==1){
			%hair="HairShounen";
		}
		if($Pref::Avatar::hat==2){
			%hair="HairShounen2";
		}
		if($Pref::Avatar::hat==3){
			%hair="HairEmo";
		}
		if($Pref::Avatar::hat==4){
			%hair="HairMullet";
		}
		if($Pref::Avatar::hat==5){
			%hair="HairOld";
			%hasPin = 1;
			%Pin = "OldPin";
		}
		if($Pref::Avatar::hat==6){
			%hair="HairSide";
		}
		if($Pref::Avatar::hat==7){
			%hair="HairBuzz";
		}
		Avatar_Preview.unHideNode("", %hair);
		Avatar_Preview.setNodeColor("", %hair, $Pref::Avatar::hatColor);
		if(%hasPin==1){
			Avatar_Preview.unHideNode("", %pin);
			Avatar_Preview.setNodeColor("", %pin, $Pref::Avatar::accentColor);
		}
	}
	
	if($Pref::Avatar::accent != 0){
		if($Pref::Avatar::accent==1){
			%accent = "HatFrontBangs";
		}
		if($Pref::Avatar::accent==2){
			%accent = "HatHeadband";
		}
		if($Pref::Avatar::accent==3){
			%accent = "HatNosebleed";
		}
		
		Avatar_Preview.unHideNode("", %accent);
		Avatar_Preview.setNodeColor("", %accent, $Pref::Avatar::accentColor);
		Avatar_Preview.setNodeColor("", HatFrontBangs, $Pref::Avatar::hatColor);
		Avatar_Preview.setNodeColor("", HatNosebleed, "1 0 0 1");
	}
	
	if($Pref::Avatar::hip==1){
		Avatar_Preview.HideNode("", "pants");
		Avatar_Preview.unHideNode("", "skirt");
		Avatar_Preview.setNodeColor("", "skirt", $Pref::Avatar::hipColor);
	}
	
	Avatar_Preview.setNodeColor("", "headSkin", $Pref::Avatar::headColor);
	
	Avatar_Preview.setNodeColor("", "torsobasic", $Pref::Avatar::chestColor);
	Avatar_Preview.setNodeColor("", "pants", $Pref::Avatar::hipColor);
	
	Avatar_Preview.setNodeColor("", "rArm", $Pref::Avatar::rarmColor);
	Avatar_Preview.setNodeColor("", "lArm", $Pref::Avatar::larmColor);
	
	Avatar_Preview.setNodeColor("", "rHand", $Pref::Avatar::rhandColor);
	Avatar_Preview.setNodeColor("", "lHand", $Pref::Avatar::lhandColor);
	
	Avatar_Preview.setNodeColor("", "rShoe", $Pref::Avatar::rlegColor);
	Avatar_Preview.setNodeColor("", "lShoe", $Pref::Avatar::llegColor);
}

// Modify the avatar GUI when it loads to show different icons and nodes

package acpAvatarPreviewFix{
	function AvatarGui::onWake(%this){
		//Defer to normal routine if the playertype is Default
		if($Pref::Player::CustomPlayertype==0 || $Pref::Player::CustomPlayertype$=""){
			$AvatarHasLoadedCustom = 0;
			
			Avatar_FacePreview.visible = 1;
			Avatar_DecalPreview.visible = 1;
			
			parent::onWake(%this);
			return;
		}
		
		Avatar_FacePreview.visible = 0;
		Avatar_DecalPreview.visible = 0;
		
		//Load avatar settings
		
		if ($Avatar::NumColors $= ""){
			ColorSetGui.load();
		}
		
		AvatarGui.updateFavButtons();
		AV_FavsHelper.setVisible(0);
		
		$Old::Avatar::Accent = $pref::Avatar::Accent;
		$Old::Avatar::AccentColor = $pref::Avatar::AccentColor;
		$Old::Avatar::Authentic = $pref::Avatar::Authentic;
		$Old::Avatar::Chest = $Pref::Avatar::Chest;
		$Old::Avatar::ChestColor = $pref::Avatar::ChestColor;
		$Old::Avatar::DecalColor = $pref::Avatar::DecalColor;
		$Old::Avatar::DecalName = $Pref::Avatar::DecalName;
		$Old::Avatar::FaceColor = $pref::Avatar::FaceColor;
		$Old::Avatar::FaceName = $Pref::Avatar::FaceName;
		$Old::Avatar::Hat = $pref::Avatar::Hat;
		$Old::Avatar::HatColor = $pref::Avatar::HatColor;
		$Old::Avatar::HeadColor = $pref::Avatar::HeadColor;
		$Old::Avatar::Hip = $Pref::Avatar::Hip;
		$Old::Avatar::HipColor = $pref::Avatar::HipColor;
		$Old::Avatar::LArm = $Pref::Avatar::LArm;
		$Old::Avatar::LArmColor = $pref::Avatar::LArmColor;
		$Old::Avatar::LHand = $Pref::Avatar::LHand;
		$Old::Avatar::LHandColor = $pref::Avatar::LHandColor;
		$Old::Avatar::LLeg = $Pref::Avatar::LLeg;
		$Old::Avatar::LLegColor = $pref::Avatar::LLegColor;
		$Old::Avatar::Pack = $pref::Avatar::Pack;
		$Old::Avatar::PackColor = $pref::Avatar::PackColor;
		$Old::Avatar::RArm = $Pref::Avatar::RArm;
		$Old::Avatar::RArmColor = $pref::Avatar::RArmColor;
		$Old::Avatar::RHand = $Pref::Avatar::RHand;
		$Old::Avatar::RHandColor = $pref::Avatar::RHandColor;
		$Old::Avatar::RLeg = $Pref::Avatar::RLeg;
		$Old::Avatar::RLegColor = $pref::Avatar::RLegColor;
		$Old::Avatar::SecondPack = $Pref::Avatar::SecondPack;
		$Old::Avatar::SecondPackColor = $pref::Avatar::SecondPackColor;
		$Old::Avatar::Symmetry = $pref::Avatar::Symmetry;
		$Old::Avatar::TorsoColor = $pref::Avatar::TorsoColor;
		
		// Position camera in preview window
		
		if ($AvatarHasLoadedCustom==$Pref::Player::CustomPlayertype){
			Avatar_Preview.setCamera();
			Avatar_Preview.setCameraRot(0.3, 0.6, 2.52);
			Avatar_Preview.setOrbitDist(4.34);
			Avatar_UpdatePreview();
			return;
		}
		$AvatarHasLoaded = 0;
		$AvatarHasLoadedCustom = $Pref::Player::CustomPlayertype;
		
		// Create clickable lists of components for each node
		
		%bodytype = $AnimeCustomBodyType[$Pref::Player::CustomPlayertype];
		
		%x = getWord(Avatar_FacePreview.position, 0) + 64.0;
		%y = getWord(Avatar_FacePreview.position, 1);
		AvatarGui_DeletePartMenuCustom("Avatar_FaceMenu", "Avatar_SetFace", "base/data/shapes/player/face.ifl", %x, %y);
		%x = getWord(Avatar_DecalPreview.position, 0) + 64.0;
		%y = getWord(Avatar_DecalPreview.position, 1) - 64.0;
		AvatarGui_DeletePartMenuCustom("Avatar_DecalMenu", "Avatar_SetDecal", "base/data/shapes/player/decal.ifl", %x, %y);
		%x = getWord(Avatar_PackPreview.position, 0) + 64.0;
		%y = getWord(Avatar_PackPreview.position, 1) - 64.0;
		AvatarGui_CreatePartMenuCustom("Avatar_PackMenu", "Avatar_SetPack", "pack", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/torso.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_SecondPackPreview.position, 0) + 64.0;
		%y = getWord(Avatar_SecondPackPreview.position, 1) - 64.0;
		AvatarGui_CreatePartMenuCustom("Avatar_SecondPackMenu", "Avatar_SetSecondPack", "secondpack", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/eyes.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_HatPreview.position, 0) + 64.0;
		%y = getWord(Avatar_HatPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_HatMenu", "Avatar_SetHatCustom", "hat", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/hair.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_AccentPreview.position, 0) + 64.0;
		%y = getWord(Avatar_AccentPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_AccentMenu", "Avatar_SetAccent", "accent", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/hat.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_ChestPreview.position, 0) + 64.0;
		%y = getWord(Avatar_ChestPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_ChestMenu", "Avatar_SetChest", "chest", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/chest.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_HipPreview.position, 0) + 64.0;
		%y = getWord(Avatar_HipPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_HipMenu", "Avatar_SetHip", "hip", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/hip.txt", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype, %x, %y);
		%x = getWord(Avatar_RLegPreview.position, 0) + 64.0;
		%y = getWord(Avatar_RLegPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_RLegMenu", "Avatar_SetRLeg", "rleg", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/rleg.txt", "base/client/ui/avatarIcons/rleg", %x, %y);
		%x = getWord(Avatar_LLegPreview.position, 0) + 64.0;
		%y = getWord(Avatar_LLegPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_LLegMenu", "Avatar_SetLLeg", "lleg", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/lleg.txt", "base/client/ui/avatarIcons/lleg", %x, %y);
		%x = getWord(Avatar_RArmPreview.position, 0) + 64.0;
		%y = getWord(Avatar_RArmPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_RArmMenu", "Avatar_SetRArm", "rarm", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/rarm.txt", "base/client/ui/avatarIcons/rarm", %x, %y);
		%x = getWord(Avatar_LArmPreview.position, 0) + 64.0;
		%y = getWord(Avatar_LArmPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_LArmMenu", "Avatar_SetLArm", "larm", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/larm.txt", "base/client/ui/avatarIcons/larm", %x, %y);
		%x = getWord(Avatar_RHandPreview.position, 0) + 64.0;
		%y = getWord(Avatar_RHandPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_RHandMenu", "Avatar_SetRHand", "rhand", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/rhand.txt", "base/client/ui/avatarIcons/rhand", %x, %y);
		%x = getWord(Avatar_LHandPreview.position, 0) + 64.0;
		%y = getWord(Avatar_LHandPreview.position, 1);
		AvatarGui_CreatePartMenuCustom("Avatar_LHandMenu", "Avatar_SetLHand", "lhand", "Add-Ons/Client_AnimeCustomAvatarPreview/icons/lhand.txt", "base/client/ui/avatarIcons/lhand", %x, %y);
		
		// Create preview model
		
		if(!isObject(AnimeCustomDts)){
			new TSShapeConstructor(AnimeCustomDts){
				baseShape  = "Add-Ons/Player_AnimeCustom/Female/AnimeCustom.dts";
				sequence0  = "Add-Ons/Player_AnimeCustom/Female/pomu_root.dsq root";
				sequence1  = "Add-Ons/Player_AnimeCustom/Female/pomu_walk.dsq run";
				sequence2  = "Add-Ons/Player_AnimeCustom/Female/pomu_walk.dsq walk";
				sequence3  = "Add-Ons/Player_AnimeCustom/Female/pomu_back.dsq back";
				sequence4  = "Add-Ons/Player_AnimeCustom/Female/pomu_side.dsq side";
				sequence5  = "Add-Ons/Player_AnimeCustom/Female/pomu_crouch.dsq crouch";
				sequence6  = "Add-Ons/Player_AnimeCustom/Female/pomu_crouchrun.dsq crouchRun";
				sequence7  = "Add-Ons/Player_AnimeCustom/Female/pomu_crouchback.dsq crouchBack";
				sequence8  = "Add-Ons/Player_AnimeCustom/Female/pomu_crouchrun.dsq crouchSide";
				sequence9  = "Add-Ons/Player_AnimeCustom/Female/pomu_look.dsq look";
				sequence10 = "Add-Ons/Player_AnimeCustom/Female/pomu_headside.dsq headside";
				sequence11 = "Add-Ons/Player_AnimeCustom/Female/pomu_root.dsq headUp";
				sequence12 = "Add-Ons/Player_AnimeCustom/Female/pomu_jump.dsq jump";
				sequence13 = "Add-Ons/Player_AnimeCustom/Female/pomu_jump.dsq standjump";
				sequence14 = "Add-Ons/Player_AnimeCustom/Female/pomu_fall.dsq fall";
				sequence15 = "Add-Ons/Player_AnimeCustom/Female/pomu_root.dsq land";
				sequence16 = "Add-Ons/Player_AnimeCustom/Female/pomu_armAttack.dsq armAttack";
				sequence17 = "Add-Ons/Player_AnimeCustom/Female/pomu_armReadyLeft.dsq armReadyLeft";
				sequence18 = "Add-Ons/Player_AnimeCustom/Female/pomu_armReadyRight.dsq armReadyRight";
				sequence19 = "Add-Ons/Player_AnimeCustom/Female/pomu_armReadyBoth.dsq armReadyBoth";
				sequence20 = "Add-Ons/Player_AnimeCustom/Female/pomu_spearready.dsq spearready";  
				sequence21 = "Add-Ons/Player_AnimeCustom/Female/pomu_spearThrow.dsq spearThrow";
				sequence22 = "Add-Ons/Player_AnimeCustom/Female/pomu_talk.dsq talk";  
				sequence23 = "Add-Ons/Player_AnimeCustom/Female/pomu_death1.dsq death1"; 
				sequence24 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftUp.dsq shiftUp";
				sequence25 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftDown.dsq shiftDown";
				sequence26 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftAway.dsq shiftAway";
				sequence27 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftTo.dsq shiftTo";
				sequence28 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftLeft.dsq shiftLeft";
				sequence29 = "Add-Ons/Player_AnimeCustom/Female/pomu_shiftRight.dsq shiftRight";
				sequence30 = "Add-Ons/Player_AnimeCustom/Female/pomu_rotCW.dsq rotCW";
				sequence31 = "Add-Ons/Player_AnimeCustom/Female/pomu_rotCCW.dsq rotCCW";
				sequence32 = "Add-Ons/Player_AnimeCustom/Female/pomu_undo.dsq undo";
				sequence33 = "Add-Ons/Player_AnimeCustom/Female/pomu_plant.dsq plant";
				sequence34 = "Add-Ons/Player_AnimeCustom/Female/pomu_sit.dsq sit";
				sequence35 = "Add-Ons/Player_AnimeCustom/Female/pomu_activate.dsq wrench";
				sequence36 = "Add-Ons/Player_AnimeCustom/Female/pomu_activate.dsq activate";
				sequence37 = "Add-Ons/Player_AnimeCustom/Female/pomu_activate2.dsq activate2";
				sequence38 = "Add-Ons/Player_AnimeCustom/Female/pomu_leftrecoil.dsq leftrecoil";
				sequence39 = "Add-Ons/Player_AnimeCustom/Female/pomu_dab.dsq dab";
			};
			
			new TSShapeConstructor(AniLoliCustomDts){
				baseShape  = "Add-Ons/Player_AnimeCustom/Loli/AniLoliCustom.dts";
				sequence0  = "Add-Ons/Player_AnimeCustom/Loli/pomu_root.dsq root";
				sequence1  = "Add-Ons/Player_AnimeCustom/Loli/pomu_walk.dsq run";
				sequence2  = "Add-Ons/Player_AnimeCustom/Loli/pomu_walk.dsq walk";
				sequence3  = "Add-Ons/Player_AnimeCustom/Loli/pomu_back.dsq back";
				sequence4  = "Add-Ons/Player_AnimeCustom/Loli/pomu_side.dsq side";
				sequence5  = "Add-Ons/Player_AnimeCustom/Loli/pomu_crouch.dsq crouch";
				sequence6  = "Add-Ons/Player_AnimeCustom/Loli/pomu_crouchrun.dsq crouchRun";
				sequence7  = "Add-Ons/Player_AnimeCustom/Loli/pomu_crouchback.dsq crouchBack";
				sequence8  = "Add-Ons/Player_AnimeCustom/Loli/pomu_crouchrun.dsq crouchSide";
				sequence9  = "Add-Ons/Player_AnimeCustom/Loli/pomu_look.dsq look";
				sequence10 = "Add-Ons/Player_AnimeCustom/Loli/pomu_headside.dsq headside";
				sequence11 = "Add-Ons/Player_AnimeCustom/Loli/pomu_root.dsq headUp";
				sequence12 = "Add-Ons/Player_AnimeCustom/Loli/pomu_jump.dsq jump";
				sequence13 = "Add-Ons/Player_AnimeCustom/Loli/pomu_jump.dsq standjump";
				sequence14 = "Add-Ons/Player_AnimeCustom/Loli/pomu_fall.dsq fall";
				sequence15 = "Add-Ons/Player_AnimeCustom/Loli/pomu_root.dsq land";
				sequence16 = "Add-Ons/Player_AnimeCustom/Loli/pomu_armAttack.dsq armAttack";
				sequence17 = "Add-Ons/Player_AnimeCustom/Loli/pomu_armReadyLeft.dsq armReadyLeft";
				sequence18 = "Add-Ons/Player_AnimeCustom/Loli/pomu_armReadyRight.dsq armReadyRight";
				sequence19 = "Add-Ons/Player_AnimeCustom/Loli/pomu_armReadyBoth.dsq armReadyBoth";
				sequence20 = "Add-Ons/Player_AnimeCustom/Loli/pomu_spearready.dsq spearready";  
				sequence21 = "Add-Ons/Player_AnimeCustom/Loli/pomu_spearThrow.dsq spearThrow";
				sequence22 = "Add-Ons/Player_AnimeCustom/Loli/pomu_talk.dsq talk";  
				sequence23 = "Add-Ons/Player_AnimeCustom/Loli/pomu_death1.dsq death1"; 
				sequence24 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftUp.dsq shiftUp";
				sequence25 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftDown.dsq shiftDown";
				sequence26 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftAway.dsq shiftAway";
				sequence27 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftTo.dsq shiftTo";
				sequence28 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftLeft.dsq shiftLeft";
				sequence29 = "Add-Ons/Player_AnimeCustom/Loli/pomu_shiftRight.dsq shiftRight";
				sequence30 = "Add-Ons/Player_AnimeCustom/Loli/pomu_rotCW.dsq rotCW";
				sequence31 = "Add-Ons/Player_AnimeCustom/Loli/pomu_rotCCW.dsq rotCCW";
				sequence32 = "Add-Ons/Player_AnimeCustom/Loli/pomu_undo.dsq undo";
				sequence33 = "Add-Ons/Player_AnimeCustom/Loli/pomu_plant.dsq plant";
				sequence34 = "Add-Ons/Player_AnimeCustom/Loli/pomu_sit.dsq sit";
				sequence35 = "Add-Ons/Player_AnimeCustom/Loli/pomu_activate.dsq wrench";
				sequence36 = "Add-Ons/Player_AnimeCustom/Loli/pomu_activate.dsq activate";
				sequence37 = "Add-Ons/Player_AnimeCustom/Loli/pomu_activate2.dsq activate2";
				sequence38 = "Add-Ons/Player_AnimeCustom/Loli/pomu_leftrecoil.dsq leftrecoil";
				sequence39 = "Add-Ons/Player_AnimeCustom/Loli/pomu_dab.dsq dab";
			};
			
			new TSShapeConstructor(AniMaleCustomDts){
				baseShape  = "Add-Ons/Player_AnimeCustom/Male/AniMaleCustom.dts";
				sequence0  = "Add-Ons/Player_AnimeCustom/Male/pomu_root.dsq root";
				sequence1  = "Add-Ons/Player_AnimeCustom/Male/pomu_walk.dsq run";
				sequence2  = "Add-Ons/Player_AnimeCustom/Male/pomu_walk.dsq walk";
				sequence3  = "Add-Ons/Player_AnimeCustom/Male/pomu_back.dsq back";
				sequence4  = "Add-Ons/Player_AnimeCustom/Male/pomu_side.dsq side";
				sequence5  = "Add-Ons/Player_AnimeCustom/Male/pomu_crouch.dsq crouch";
				sequence6  = "Add-Ons/Player_AnimeCustom/Male/pomu_crouchrun.dsq crouchRun";
				sequence7  = "Add-Ons/Player_AnimeCustom/Male/pomu_crouchback.dsq crouchBack";
				sequence8  = "Add-Ons/Player_AnimeCustom/Male/pomu_crouchrun.dsq crouchSide";
				sequence9  = "Add-Ons/Player_AnimeCustom/Male/pomu_look.dsq look";
				sequence10 = "Add-Ons/Player_AnimeCustom/Male/pomu_headside.dsq headside";
				sequence11 = "Add-Ons/Player_AnimeCustom/Male/pomu_root.dsq headUp";
				sequence12 = "Add-Ons/Player_AnimeCustom/Male/pomu_jump.dsq jump";
				sequence13 = "Add-Ons/Player_AnimeCustom/Male/pomu_jump.dsq standjump";
				sequence14 = "Add-Ons/Player_AnimeCustom/Male/pomu_fall.dsq fall";
				sequence15 = "Add-Ons/Player_AnimeCustom/Male/pomu_root.dsq land";
				sequence16 = "Add-Ons/Player_AnimeCustom/Male/pomu_armAttack.dsq armAttack";
				sequence17 = "Add-Ons/Player_AnimeCustom/Male/pomu_armReadyLeft.dsq armReadyLeft";
				sequence18 = "Add-Ons/Player_AnimeCustom/Male/pomu_armReadyRight.dsq armReadyRight";
				sequence19 = "Add-Ons/Player_AnimeCustom/Male/pomu_armReadyBoth.dsq armReadyBoth";
				sequence20 = "Add-Ons/Player_AnimeCustom/Male/pomu_spearready.dsq spearready";  
				sequence21 = "Add-Ons/Player_AnimeCustom/Male/pomu_spearThrow.dsq spearThrow";
				sequence22 = "Add-Ons/Player_AnimeCustom/Male/pomu_talk.dsq talk";  
				sequence23 = "Add-Ons/Player_AnimeCustom/Male/pomu_death1.dsq death1"; 
				sequence24 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftUp.dsq shiftUp";
				sequence25 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftDown.dsq shiftDown";
				sequence26 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftAway.dsq shiftAway";
				sequence27 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftTo.dsq shiftTo";
				sequence28 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftLeft.dsq shiftLeft";
				sequence29 = "Add-Ons/Player_AnimeCustom/Male/pomu_shiftRight.dsq shiftRight";
				sequence30 = "Add-Ons/Player_AnimeCustom/Male/pomu_rotCW.dsq rotCW";
				sequence31 = "Add-Ons/Player_AnimeCustom/Male/pomu_rotCCW.dsq rotCCW";
				sequence32 = "Add-Ons/Player_AnimeCustom/Male/pomu_undo.dsq undo";
				sequence33 = "Add-Ons/Player_AnimeCustom/Male/pomu_plant.dsq plant";
				sequence34 = "Add-Ons/Player_AnimeCustom/Male/pomu_sit.dsq sit";
				sequence35 = "Add-Ons/Player_AnimeCustom/Male/pomu_activate.dsq wrench";
				sequence36 = "Add-Ons/Player_AnimeCustom/Male/pomu_activate.dsq activate";
				sequence37 = "Add-Ons/Player_AnimeCustom/Male/pomu_activate2.dsq activate2";
				sequence38 = "Add-Ons/Player_AnimeCustom/Male/pomu_leftrecoil.dsq leftrecoil";
				sequence39 = "Add-Ons/Player_AnimeCustom/Male/pomu_dab.dsq dab";
				sequence39 = "Add-Ons/Player_AnimeCustom/Male/pomu_narudash.dsq narudash";
			}; 
			
		}
		
		if($Pref::Player::CustomPlayertype==1){
			%prevobj = "Add-Ons/Player_AnimeCustom/Female/AnimeCustom.dts";
		}else if($Pref::Player::CustomPlayertype==2){
			%prevobj = "Add-Ons/Player_AnimeCustom/Loli/AniLoliCustom.dts";
		}else if($Pref::Player::CustomPlayertype==3){
			%prevobj = "Add-Ons/Player_AnimeCustom/Male/AniMaleCustom.dts";
		}
		
		Avatar_Preview.setObject("", %prevobj, "", 100);
		Avatar_Preview.setSequence("", 0, headup, 0);
		Avatar_Preview.setThreadPos("", 0, 0);
		Avatar_Preview.setSequence("", 1, run, 0.85);
		Avatar_Preview.setCameraRot(0.3, 0.6, 2.52);
		Avatar_Preview.setOrbitDist(4.34);
		
		// Load clan tag fields
		
		Avatar_Prefix.setText($Pref::Player::ClanPrefix);
		Avatar_Suffix.setText($Pref::Player::ClanSuffix);
		
		// Update preview model
		
		Avatar_UpdatePreview();
	}
	
	function Avatar_UpdatePreview(){
		// Defer to normal routine if player type is Default
		if($Pref::Player::CustomPlayertype==0){
			parent::Avatar_UpdatePreview();
			return;
		}
		
		// Apply node visibility and colors to preview model
		
		if($Pref::Player::CustomPlayertype==1){
			acpFixFemale();
		}else if($Pref::Player::CustomPlayertype==2){
			acpFixLoli();
		}else if($Pref::Player::CustomPlayertype==3){
			acpFixMale();
		}
		
		// Apply colors to icons
		
		Avatar_Preview.setIflFrame("", decal, $pref::Avatar::DecalColor);
		Avatar_Preview.setIflFrame("", face, $pref::Avatar::FaceColor);
		Avatar_TorsoColor.setColor($pref::Avatar::TorsoColor);
		Avatar_ChestPreview.setColor($pref::Avatar::TorsoColor);
		Avatar_ColorAllIcons(Avatar_ChestMenuBG, $pref::Avatar::TorsoColor);
		Avatar_DecalBG.setColor($pref::Avatar::TorsoColor);
		Avatar_DecalMenuBG.setColor($pref::Avatar::TorsoColor);
		Avatar_HeadColor.setColor($pref::Avatar::HeadColor);
		Avatar_HeadBG.setColor($pref::Avatar::HeadColor);
		Avatar_faceMenuBG.setColor($pref::Avatar::HeadColor);
		Avatar_HipColor.setColor($pref::Avatar::HipColor);
		Avatar_HipPreview.setColor($pref::Avatar::HipColor);
		Avatar_ColorAllIcons(Avatar_HipMenuBG, $pref::Avatar::HipColor);
		Avatar_LeftArmColor.setColor($pref::Avatar::LArmColor);
		Avatar_LArmPreview.setColor($pref::Avatar::LArmColor);
		Avatar_ColorAllIcons(Avatar_LarmMenuBG, $pref::Avatar::LArmColor);
		Avatar_LeftHandColor.setColor($pref::Avatar::LHandColor);
		Avatar_LHandPreview.setColor($pref::Avatar::LHandColor);
		Avatar_ColorAllIcons(Avatar_LHandMenuBG, $pref::Avatar::LHandColor);
		Avatar_LeftLegColor.setColor($pref::Avatar::LLegColor);
		Avatar_LLegPreview.setColor($pref::Avatar::LLegColor);
		Avatar_ColorAllIcons(Avatar_LLegMenuBG, $pref::Avatar::LLegColor);
		Avatar_RightArmColor.setColor($pref::Avatar::RArmColor);
		Avatar_RArmPreview.setColor($pref::Avatar::RArmColor);
		Avatar_ColorAllIcons(Avatar_RarmMenuBG, $pref::Avatar::RArmColor);
		Avatar_RightHandColor.setColor($pref::Avatar::RHandColor);
		Avatar_RHandPreview.setColor($pref::Avatar::RHandColor);
		Avatar_ColorAllIcons(Avatar_RHandMenuBG, $pref::Avatar::RHandColor);
		Avatar_RightLegColor.setColor($pref::Avatar::RLegColor);
		Avatar_RLegPreview.setColor($pref::Avatar::RLegColor);
		Avatar_ColorAllIcons(Avatar_RLegMenuBG, $pref::Avatar::RLegColor);
		Avatar_HatColor.setColor($pref::Avatar::HatColor);
		Avatar_HatPreview.setColor($pref::Avatar::HatColor);
		Avatar_ColorAllIcons(Avatar_HatMenuBG, $pref::Avatar::HatColor);
		Avatar_AccentColor.setColor($pref::Avatar::AccentColor);
		Avatar_AccentPreview.setColor($pref::Avatar::AccentColor);
		Avatar_ColorAllIcons(Avatar_AccentMenuBG, $pref::Avatar::AccentColor);
		Avatar_PackColor.setColor($pref::Avatar::PackColor);
		Avatar_PackPreview.setColor($pref::Avatar::PackColor);
		Avatar_ColorAllIcons(Avatar_PackMenuBG, $pref::Avatar::PackColor);
		Avatar_SecondPackColor.setColor($pref::Avatar::SecondPackColor);
		Avatar_SecondPackPreview.setColor($pref::Avatar::SecondPackColor);
		Avatar_ColorAllIcons(Avatar_SecondPackMenuBG, $pref::Avatar::SecondPackColor);
		
		// Set icons
		
		%bodytype = $AnimeCustomBodyType[$Pref::Player::CustomPlayertype];
		%iconDirCustom = "Add-Ons/Client_AnimeCustomAvatarPreview/icons/" @ %bodytype @ "/";
		
		Avatar_PackPreview.setBitmap(%iconDirCustom @ $animeCustom["Torso", %bodytype, $pref::Avatar::Pack]);
		Avatar_SecondPackPreview.setBitmap(%iconDirCustom @ $animeCustom["Eyes", %bodytype, $Pref::Avatar::SecondPack]);
		Avatar_HatPreview.setBitmap(%iconDirCustom @ $animeCustom["Hair", %bodytype, $pref::Avatar::Hat]);
		Avatar_HipPreview.setBitmap(%iconDirCustom @ $animeCustom["Hip", %bodytype, $Pref::Avatar::Hip]);
		Avatar_AccentPreview.setBitmap(%iconDirCustom @ $animeCustom["Hat", %bodytype, $pref::Avatar::Accent]);
		Avatar_ChestPreview.setBitmap(%iconDirCustom @ $animeCustom["Chest", %bodytype, $pref::Avatar::Chest]);
		
		%iconDir = "base/client/ui/avatarIcons/";
		
		Avatar_LArmPreview.setBitmap(%iconDir @ "Larm/larm");
		Avatar_LHandPreview.setBitmap(%iconDir @ "Lhand/lhand");
		Avatar_LLegPreview.setBitmap(%iconDir @ "Lleg/lshoe");
		Avatar_RArmPreview.setBitmap(%iconDir @ "Rarm/rarm");
		Avatar_RHandPreview.setBitmap(%iconDir @ "Rhand/rhand");
		Avatar_RLegPreview.setBitmap(%iconDir @ "Rleg/rshoe");
	}
};

activatePackage(acpAvatarPreviewFix);


// Copy of default function to create part menus, allowing custom icon directories

function AvatarGui_CreatePartMenuCustom(%name, %cmdString, %base, %filename, %iconDir, %xPos, %yPos){
	if(isObject(%name)){
		eval(%name @ ".delete();");
	}
	
	%newScroll = new GuiScrollCtrl(""){};
	%newScroll.vScrollBar = "alwaysOn";
	%newScroll.hScrollBar = "alwaysOff";
	%newScroll.setProfile(ColorScrollProfile);
	Avatar_Window.add(%newScroll);
	%w = 64.0 + 12.0;
	%h = 64;
	%newScroll.resize(%xPos, %yPos, %w, %h);
	%newScroll.setName(%name);
	Avatar_Window.schedule(10, pushToBack, %name);
	
	%newBox = new GuiBitmapCtrl(""){};
	%newScroll.add(%newBox);
	%newBox.setBitmap("base/client/ui/btnDecalBG");
	%newBox.wrap = 1;
	%newBox.resize(0, 0, 64, 64);
	%newBox.setName("Avatar_" @ %base @ "MenuBG");
	
	%file = new FileObject(""){};
	%file.openForRead(%filename);
	%itemCount = 0;
	%varString = "$" @ %base;
	
	echo(%name SPC %filename);
	
	%line = %file.readLine();
	while(%line !$= ""){
		%newImage = new GuiBitmapCtrl(""){};
		%newBox.add(%newImage);
		%newImage.keepCached = 1;
		%newImage.setBitmap(%iconDir @ "/" @ %line);
		%x = (%itemCount%4) * 64.0;
		%y = mFloor(%itemCount / 4.0) * 64.0;
		%newImage.resize(%x, %y, 64, 64);
		%newButton = new GuiBitmapButtonCtrl(""){
			profile = "BlockButtonProfile";
		};
		%newBox.add(%newButton);
		%newButton.setBitmap("base/client/ui/btnDecal");
		%newButton.setText(" ");
		%newButton.resize(%x, %y, 64, 64);
		%newButton.command = %cmdString @ "(" @ %itemCount @ "," @ %newImage @ ");";
		
		echo(%name SPC %cmdString SPC %base SPC %filename SPC %iconDir SPC %xpos SPC %ypos);
		
		%cmd = %varString @ "[" @ %itemCount @ "] = " @ %line @ ";";
		eval(%cmd);
		
		%itemCount = %itemCount + 1.0;
		%line = %file.readLine();
	}
	$num[%base] = %itemCount;
	%file.close();
	%file.delete();
	
	if(%itemCount >= 4.0){
		%w = 4.0 * 64.0;
	}else{
		%w = %itemCount * 64.0;
	}
	%h = (mFloor(%itemCount / 4.0 + 0.95)) * 64.0;
	%newBox.resize(0, 0, %w, %h);
	if(%yPos + %h > 480.0){
		%h = (mFloor((480.0 - %yPos) / 64.0)) * 64.0;
	}
	%newScroll.resize(%xPos, %yPos, %w + 12.0, %h);
	%newScroll.setVisible(0);
}

// Copy of default function to create part menus, to create an empty menu

function AvatarGui_DeletePartMenuCustom(%name, %cmdString, %filename, %xPos, %yPos){
	if(isObject(%name)){
		eval(%name @ ".delete();");
	}
	
	%newScroll = new GuiScrollCtrl(""){};
	%newScroll.vScrollBar = "alwaysOn";
	%newScroll.hScrollBar = "alwaysOff";
	%newScroll.setProfile(ColorScrollProfile);
	Avatar_Window.add(%newScroll);
	%w = 64.0 + 12.0;
	%h = 64;
	%newScroll.resize(%xPos, %yPos, %w, %h);
	%newScroll.setName(%name);
	Avatar_Window.schedule(10, pushToBack, %name);
	
	%newBox = new GuiBitmapCtrl(""){};
	%newScroll.add(%newBox);
	%newBox.setBitmap("base/client/ui/btnDecalBG");
	%newBox.wrap = 1;
	%newBox.resize(0, 0, 64, 64);
	%newBox.setName("Avatar_" @ fileBase(%filename) @ "MenuBG");
	
	%w = 0;
	%h = 0;
	%newBox.resize(0, 0, %w, %h);
	%newScroll.resize(%xPos, %yPos, %w + 12.0, %h);
	%newScroll.setVisible(0);
}

// Copy of default function to set the hat, which does not restrict accent options based on hat choice

function Avatar_SetHatCustom(%index, %imageObj){
	$pref::Avatar::Hat = %index;
	Avatar_HatMenu.setVisible(0);
	Avatar_UpdatePreview();
}
